#include<stdio.h>
#include<conio.h>
#include <stdint.h>
#include "cbuffer.h"

cbuffer_t cb;
uint8_t cb_buff[10];
int main()
{
   
    uint8_t a[] = {0, 1, 2, 3};
    cb_init(&cb, cb_buff,5);
    cb_write(&cb, a, 4);

    printf("\nWrite\t");
    for (int i = 0; i < 5; i++)
    {
        printf("%d\t", *((uint8_t*)cb_buff + i));
    }

    uint8_t c[3] = { 0 };
    cb_read(&cb, c, 3);
    printf("\nRead\t");
    for (int i = 0; i < 3; i++)
    {
        printf("%d\t", c[i]);
    }

    uint8_t b[] = { 4,5,6,7,8};
    cb_write(&cb, b,5);
    printf("\nWrite\t");
    for (int i = 0; i < 5; i++)
    {
        printf("%d\t", *((uint8_t*)cb_buff + i));
    }

    uint8_t d[8] = { 0 };
    cb_read(&cb, d, 8);
     printf("\nRead\t");
    for (int i = 0; i < 8; i++)
    {
        printf("%d\t", d[i]);
    }

    printf("\nData count:\t%d",cb_data_count(&cb));
    printf("\nSpace count:\t%d",cb_space_count(&cb));
    cb_clear(&cb);

    uint8_t l[5] = { 0 };
    cb_read(&cb, l, 3);
    printf("\nRead\t");
    for (int i = 0; i < 5; i++)
    {
        printf("%d\t", l[i]);
    }
    printf("\nData count:\t%d",cb_data_count(&cb));
    printf("\nSpace count:\t%d",cb_space_count(&cb));
    return 0;
}