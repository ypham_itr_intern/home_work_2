#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include "cbuffer.h"

void cb_init(cbuffer_t* cb, void* buf, uint32_t size)
{
    cb->data = (uint8_t*)buf;
    cb->size = size;
    cb->writer = 0;
    cb->reader = 0;
    cb->overflow = 0;
    cb->active = true;
}
void cb_clear(cbuffer_t* cb)
{
    cb->writer = 0;
    cb->reader = 0;
    cb->overflow = 0;
    memset(cb->data,0,cb->size);
}
uint32_t cb_write(cbuffer_t* cb, void* buf, uint32_t nbytes)
{
    int count = 0;
    for (int i = 0; i < nbytes; i++)
    {
        if (cb->writer == cb->size)
        {
            cb->writer = 0;
            count ++;
        }
        if (count!=0)
        {
            cb->overflow++;
        }
        cb->data[cb->writer] = *((uint8_t*)buf + i);
        cb->writer++;
    }
    return nbytes;
}
uint32_t cb_read(cbuffer_t* cb, void* buf, uint32_t nbytes)
{
    int temp = cb->overflow;
    for (int i = 0; i < nbytes; i++)
    {
        if (cb->reader == cb->size && temp != 0)
        {
            cb->reader = 0;
            temp = 0;
        }
        if (cb->reader == cb->writer && temp == 0)
        {
           return 0;
        }
        *((uint8_t*)buf + i) = cb->data[cb->reader];
        cb->reader++;
    }
    return nbytes;
}
uint32_t cb_data_count(cbuffer_t* cb)
{
    if(cb->overflow != 0)
    {
        return cb->overflow+cb->size;
    }
    return cb->writer;
}
uint32_t cb_space_count(cbuffer_t* cb)
{
    if(cb->overflow != 0)
    {
        return  0;
    }
    else
    {
        return (cb->size - cb->writer);
    }
}
